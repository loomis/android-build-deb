#!/bin/bash

set -e
set -x

export LANG=C
export USER=build
unset _JAVA_OPTIONS
export BUILD_NUMBER=$(date --utc +%Y.%m.%d.%H.%M.%S)
export DISPLAY_BUILD_NUMBER=true

source build/envsetup.sh
choosecombo release aosp_deb userdebug

make dist -j8
