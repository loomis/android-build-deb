FROM jamesloomis/android-build

RUN mkdir /home/build/android
ADD build.sh /home/build/android/build.sh
WORKDIR /home/build/android

RUN repo init -q -u https://gitlab.com/loomis/platform_manifest.git -b marshmallow-mr2-release
RUN repo sync -q -j8 -c

RUN ./build.sh
